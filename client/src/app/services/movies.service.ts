import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';

@Injectable({
  providedIn: 'root'
})
export class MoviesService {

  constructor(private httpClient: HttpClient) { }

  getMovie(searchedMovie: string): Observable<any> {
    return this.httpClient.get<any>(`${environment.apiUrl}search/movie?api_key=${environment.apiKey}&language=en-US&query=${searchedMovie}`);
  }
}
