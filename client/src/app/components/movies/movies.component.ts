import { Component, OnInit, Input } from '@angular/core';
import { MoviesService } from 'src/app/services/movies.service';
import { FormControl, FormGroup } from '@angular/forms'
import { Movies } from 'src/app/models/Movies';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss']
})
export class MoviesComponent implements OnInit {
  movieForm = new FormGroup({
    movie: new FormControl(''),
  });

  movies = {
    results: []
  };

  constructor(private MoviesService: MoviesService) { }

  ngOnInit(): void {
  }

  onSubmit() {
    const searchedMovie = this.movieForm.value.movie;
    this.searchMovie(searchedMovie)
  }

  searchMovie(searchedMovie: string) {
    this.MoviesService.getMovie(searchedMovie).subscribe(data => {
      this.movies.results = data['results'];
    });

    return this.movies;
  }
}
