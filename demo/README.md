# Server

## Development server

Run `./mvnw install`

Run `./mvnw generate-resources`

Run `mvn spring-boot:run` run server. Navigate to `http://localhost:8080/`.

For more informations : https://spring.io/projects/spring-boot
